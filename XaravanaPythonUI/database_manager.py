import pyodbc

class DBManager:
    def get_connection(self):
        self.conn = pyodbc.connect("DRIVER={SQL Server};SERVER=localhost;DATABASE=XaravanDB;Trusted_Connection=yes;")
        self.cur = self.conn.cursor()

    def close_connection(self):
        self.cur.close()
        del self.cur
        self.conn.close()

    def __init__(self):
        self.conn = None
        self.cur = None

        self.capital_id = None
        self.worker_id = None


    def is_worker_available(self):
        self.get_connection()
        sql = """\
                        DECLARE @return_value int;
                        EXEC    @return_value = [dbo].[is_worker_available] ?;
                        SELECT  'Return Value' = @return_value;
                        """
        crsr = self.cur.execute(sql, str(self.worker_id))
        result = crsr.fetchall()[0][0]
        self.close_connection()
        return result

    def start_new_game(self):
        self.get_connection()
        print("starting a new game")
        sql = """\
                DECLARE @return_value int;
                EXEC    @return_value = [dbo].[start_game];
                SELECT  'Return Value' = @return_value;
                """
        crsr = self.cur.execute(sql)
        self.capital_id = crsr.fetchall()[0][0]

        sql = """\
                DECLARE @return_value int;
                EXEC    @return_value = [dbo].[get_capital_worker] ?;
                SELECT  'Return Value' = @return_value;
                """
        crsr = self.cur.execute(sql, str(self.capital_id))
        self.worker_id = crsr.fetchall()[0][0]

        print("capital id is: " + str(self.capital_id))
        print("worker id is:" + str(self.worker_id))


        self.close_connection()
        return 0

    def end_the_game(self):
        self.get_connection()
        print("ending the game and deleting all info")
        #sql = """\
        #                EXEC    [dbo].[end_game];
        #                """
        #crsr = self.cur.execute(sql)
        #self.capital_id = crsr.fetchall()[0][0]
        self.close_connection()
        return 0

    def upgrade_wood_producer(self, aspect):
        self.get_connection()
        print("upgrading wood producer")

        sql = """\
                DECLARE @return_value int;
                DECLARE @producer_id int;
                DECLARE @upgradable_id int;
                EXEC @producer_id = get_producer_id ?, 'wood';
                EXEC @upgradable_id = get_producer_upgradable_id @producer_id;
                EXEC @return_value = start_upgrade @upgradable_id, ?;
                SELECT  'Return Value' = @return_value;
                """
        crsr = self.cur.execute(sql, str(self.capital_id), str(aspect))
        result = crsr.fetchone()[0]
        self.close_connection()
        return int(result)

    def upgrade_food_producer(self, aspect):
        self.get_connection()
        print("upgrading food producer")
        sql = """\
                        DECLARE @return_value int;
                        EXEC @producer_id = get_producer_id ?, 'food';
                        EXEC @upgradable_id = get_producer_upgradable_id @producer_id;
                        EXEC @check_upgrade = start_upgrade @upgradable_id, ?;
                        SELECT  'Return Value' = @return_value;
                        """
        crsr = self.cur.execute(sql, self.capital_id, aspect)
        result = crsr.fetchall()[0][0]
        self.close_connection()
        return int(result)

    def upgrade_iron_producer(self, aspect):
        self.get_connection()
        print("upgrading iron producer")
        sql = """\
                                DECLARE @return_value int;
                                EXEC @producer_id = get_producer_id ?, 'iron';
                                EXEC @upgradable_id = get_producer_upgradable_id @producer_id;
                                EXEC @check_upgrade = start_upgrade @upgradable_id, ?;
                                SELECT  'Return Value' = @return_value;
                                """
        crsr = self.cur.execute(sql, self.capital_id, aspect)
        result = crsr.fetchall()[0][0]
        self.close_connection()
        return int(result)

    def upgrade_stone_producer(self, aspect):
        self.get_connection()
        print("upgrading stone producer")
        sql = """\
                                DECLARE @return_value int;
                                EXEC @producer_id = get_producer_id ?, 'stone';
                                EXEC @upgradable_id = get_producer_upgradable_id @producer_id;
                                EXEC @check_upgrade = start_upgrade @upgradable_id, ?;
                                SELECT  'Return Value' = @return_value;
                                """
        crsr = self.cur.execute(sql, self.capital_id, aspect)
        result = crsr.fetchall()[0][0]
        self.close_connection()
        return int(result)

    def upgrade_wood_warehouse(self):
        self.get_connection()
        print("upgrading wood warehouse")

        self.close_connection()
        return 0

    def upgrade_food_warehouse(self):
        print("upgrading food warehouse")
        return 0

    def upgrade_iron_warehouse(self):
        print("upgrading iron warehouse")
        return 0

    def upgrade_stone_warehouse(self):
        print("upgrading stone warehouse")
        return 0

    def upgrade_capital(self):
        self.get_connection()
        print("upgrading capital")
        sql = """\
                        EXEC    [dbo].[upgrade_turn] ?;
                        """
        self.cur.execute(sql, self.capital_id)
        self.close_connection()
        return 1


    def upgrade_archer_tower(self):
        print("upgrading archer tower")
        return 0


    def upgrade_fire_tower(self):
        print("upgrading fire tower")
        return 0


    def upgrade_arrow_tower(self):
        print("upgrading arrow tower")
        return 0


    def upgrade_catapult_tower(self):
        print("upgrading catapult tower")
        return 0


    def upgrade_trench(self):
        print("upgrading trench")
        return 0


    def upgrade_whirlpool(self):
        print("upgrading whirlpool")
        return 0


    def upgrade_wall(self):
        print("upgrading wall")
        return 0

    def discharge_food(self):
        self.get_connection()
        print("discharging food")
        self.cur.execute("EXEC discharge_food " + str(self.capital_id))
        self.close_connection()
        return 1

    def discharge_wood(self):
        self.get_connection()
        print("discharging wood")
        self.cur.execute("EXEC discharge_wood " + str(self.capital_id))
        self.close_connection()
        return 1

    def discharge_iron(self):
        self.get_connection()
        print("discharging iron")
        self.cur.execute("EXEC discharge_iron " + str(self.capital_id))
        self.close_connection()
        return 1

    def discharge_stone(self):
        self.get_connection()
        print("discharging stone")
        self.cur.execute("EXEC discharge_stone " + str(self.capital_id))
        self.close_connection()
        return 1

    def get_status(self):
        self.get_connection()
        print("getting status")
        sql = "SELECT * FROM defensives d WHERE d.capital_id = " + str(self.capital_id)
        print(sql)
        crsr = self.cur.execute(sql)
        defensives = crsr.fetchall()
        for row in defensives:
            print(row)
        sql = "SELECT * FROM Producers d WHERE d.capital_id = " + str(self.capital_id)
        print(sql)
        crsr = self.cur.execute(sql)
        defensives = crsr.fetchall()
        for row in defensives:
            print(row)
        self.close_connection()
        return str(defensives)