from tkinter import *
from tkinter import messagebox
from database_manager import *

root = Tk()
DBM = DBManager()

startButton = Button(root, text="START", fg="red")

''' menu buttons '''
upgradeButton = Button(root, text="UPGRADE", fg="blue")
statusButton = Button(root, text="STATUS", fg="blue")
attackButton = Button(root, text="ATTACK", fg="blue")
dischargeButton = Button(root, text="DISCHARGE", fg="blue")

''' upgrade buttons '''
workerIsBusyLabel = Label(root, text="Worker is busy.")
upgradeCapitalButton = Button(root, text="UPGRADE CAPITAL", fg="green")

upgradeWoodProducerButton = Button(root, text="UPGRADE WOOD PRODUCER", fg="green")
upgradeFoodProducerButton = Button(root, text="UPGRADE FOOD PRODUCER", fg="green")
upgradeStoneProducerButton = Button(root, text="UPGRADE STONE PRODUCER", fg="green")
upgradeIronProducerButton = Button(root, text="UPGRADE IRONE PRODUCER", fg="green")

upgradeWoodWarehouseButton = Button(root, text="UPGRADE WOOD WAREHOUSE", fg="purple")
upgradeFoodWarehouseButton = Button(root, text="UPGRADE FOOD WAREHOUSE", fg="purple")
upgradeStoneWarehouseButton = Button(root, text="UPGRADE STONE WAREHOUSE", fg="purple")
upgradeIronWarehouseButton = Button(root, text="UPGRADE IRON WAREHOUSE", fg="purple")

upgradeArcherTowerButton = Button(root, text="UPGRADE ARCHER TOWER", fg="black")
upgradeFireTowerButton = Button(root, text="UPGRADE FIRE TOWER", fg="black")
upgradeArrowTowerButton = Button(root, text="UPGRADE ARROW TOWER", fg="black")
upgradeCatapultTowerButton = Button(root, text="UPGRADE CATAPULT TOWER", fg="black")

upgradeTrenchButton = Button(root, text="UPGRADE TRENCH", fg="gold")
upgradeWhirlpoolButton = Button(root, text="UPGRADE WHIRLPOOL", fg="gold")
upgradeWallButton = Button(root, text="UPGRADE WALL", fg="gold")

''' back/exit buttons '''

backButton = Button(root, text="<-BACK", fg="red")
exitButton = Button(root, text="EXIT", fg="red")

''' discharge buttons '''

dischargeWoodButton = Button(root, text="DISCHARGE WOOD PRODUCER")
dischargeFoodButton = Button(root, text="DISCHARGE FOOD PRODUCER")
dischargeStoneButton = Button(root, text="DISCHARGE STONE PRODUCER")
dischargeIronButton = Button(root, text="DISCHARGE IRON PRODUCER")


''' status label '''
statusLabel = Label(root, text="test")

''' upgrade info '''
upgradeAspect = Entry(root)

def show_start_page(event):
    upgradeButton.forget()
    statusButton.forget()
    attackButton.forget()
    dischargeButton.forget()
    backButton.forget()
    startButton.pack()


def show_main_page(event):
    global statusLabel
    upgradeButton.pack()
    statusButton.pack()
    attackButton.pack()
    dischargeButton.pack()
    exitButton.pack()

    backButton.forget()
    hide_upgrade_menu()
    hide_discharge_menu()
    statusLabel.forget()


def exit_game(event):
    root.destroy()


def start_new_game(event):
    ''' add procedure call to start the game '''
    show_main_page(None)
    startButton.forget()
    DBM.start_new_game()


def show_status(event):
    ''' call status procedure based on capital id '''
    global statusLabel
    upgradeButton.forget()
    statusButton.forget()
    attackButton.forget()
    dischargeButton.forget()
    exitButton.forget()
    startButton.forget()
    backButton.pack()

    statusLabel = Label(root, text=DBM.get_status())
    statusLabel.pack()


def upgrade_menu(event):
    result = DBM.is_worker_available()
    if result == 1:
        workerIsBusyLabel.pack()
    else:
        show_upgrade_menu()
    backButton.pack()
    upgradeButton.forget()
    statusButton.forget()
    attackButton.forget()
    dischargeButton.forget()
    exitButton.forget()
    ''' call procedure to show available upgrades '''


def attack_menu(event):
    backButton.pack()
    upgradeButton.forget()
    statusButton.forget()
    attackButton.forget()
    dischargeButton.forget()
    exitButton.forget()


def discharge_menu(event):
    backButton.pack()
    show_discharge_menu()
    upgradeButton.forget()
    statusButton.forget()
    attackButton.forget()
    dischargeButton.forget()
    exitButton.forget()


def show_upgrade_menu():
    upgradeAspect.pack()
    upgradeCapitalButton.pack()
    upgradeWoodProducerButton.pack()
    upgradeFoodProducerButton.pack()
    upgradeStoneProducerButton.pack()
    upgradeIronProducerButton.pack()

    upgradeWoodWarehouseButton.pack()
    upgradeFoodWarehouseButton.pack()
    upgradeStoneWarehouseButton.pack()
    upgradeIronWarehouseButton.pack()

    upgradeArcherTowerButton.pack()
    upgradeFireTowerButton.pack()
    upgradeArrowTowerButton.pack()
    upgradeCatapultTowerButton.pack()

    upgradeTrenchButton.pack()
    upgradeWhirlpoolButton.pack()
    upgradeWallButton.pack()


def hide_upgrade_menu():
    upgradeAspect.forget()
    upgradeCapitalButton.forget()
    upgradeWoodProducerButton.forget()
    upgradeFoodProducerButton.forget()
    upgradeStoneProducerButton.forget()
    upgradeIronProducerButton.forget()

    upgradeWoodWarehouseButton.forget()
    upgradeFoodWarehouseButton.forget()
    upgradeStoneWarehouseButton.forget()
    upgradeIronWarehouseButton.forget()

    upgradeArcherTowerButton.forget()
    upgradeFireTowerButton.forget()
    upgradeArrowTowerButton.forget()
    upgradeCatapultTowerButton.forget()

    upgradeTrenchButton.forget()
    upgradeWhirlpoolButton.forget()
    upgradeWallButton.forget()


def show_discharge_menu():
    dischargeFoodButton.pack()
    dischargeIronButton.pack()
    dischargeStoneButton.pack()
    dischargeWoodButton.pack()


def hide_discharge_menu():
    dischargeFoodButton.forget()
    dischargeIronButton.forget()
    dischargeStoneButton.forget()
    dischargeWoodButton.forget()


def upgrade_wood_producer(event):
    result = DBM.upgrade_wood_producer(upgradeAspect.get())
    if result == 1:
        messagebox.showinfo("result", "upgrade successful")
    else:
        messagebox.showinfo("result", "upgrade unsuccessful")


def upgrade_food_producer(event):
    result = DBM.upgrade_food_producer(upgradeAspect.get())
    if result == 1:
        messagebox.showinfo("result", "upgrade successful")
    else:
        messagebox.showinfo("result", "upgrade unsuccessful")


def upgrade_iron_producer(event):
    result = DBM.upgrade_iron_producer(upgradeAspect.get())
    if result == 1:
        messagebox.showinfo("result", "upgrade successful")
    else:
        messagebox.showinfo("result", "upgrade unsuccessful")


def upgrade_stone_producer(event):
    result = DBM.upgrade_stone_producer(upgradeAspect.get())
    if result == 1:
        messagebox.showinfo("result", "upgrade successful")
    else:
        messagebox.showinfo("result", "upgrade unsuccessful")


def upgrade_wood_warehouse(event):
    result = DBM.upgrade_wood_warehouse(upgradeAspect.get())
    if result == 1:
        messagebox.showinfo("result", "upgrade successful")
    else:
        messagebox.showinfo("result", "upgrade unsuccessful")


def upgrade_food_warehouse(event):
    result = DBM.upgrade_food_warehouse(upgradeAspect.get())
    if result == 1:
        messagebox.showinfo("result", "upgrade successful")
    else:
        messagebox.showinfo("result", "upgrade unsuccessful")


def upgrade_iron_warehouse(event):
    result = DBM.upgrade_iron_warehouse(upgradeAspect.get())
    if result == 1:
        messagebox.showinfo("result", "upgrade successful")
    else:
        messagebox.showinfo("result", "upgrade unsuccessful")


def upgrade_stone_warehouse(event):
    result = DBM.upgrade_stone_warehouse(upgradeAspect.get())
    if result == 1:
        messagebox.showinfo("result", "upgrade successful")
    else:
        messagebox.showinfo("result", "upgrade unsuccessful")


def upgrade_capital(event):
    result = DBM.upgrade_capital()
    if result == 1:
        messagebox.showinfo("result", "upgrade successful")
    else:
        messagebox.showinfo("result", "upgrade unsuccessful")


def upgrade_whirlpool(event):
    result = DBM.upgrade_whirlpool(upgradeAspect.get())
    if result == 1:
        messagebox.showinfo("result", "upgrade successful")
    else:
        messagebox.showinfo("result", "upgrade unsuccessful")


def upgrade_trench(event):
    result = DBM.upgrade_trench(upgradeAspect.get())
    if result == 1:
        messagebox.showinfo("result", "upgrade successful")
    else:
        messagebox.showinfo("result", "upgrade unsuccessful")


def upgrade_wall(event):
    result = DBM.upgrade_wall(upgradeAspect.get())
    if result == 1:
        messagebox.showinfo("result", "upgrade successful")
    else:
        messagebox.showinfo("result", "upgrade unsuccessful")


def upgrade_archer_tower(event):
    result = DBM.upgrade_archer_tower(upgradeAspect.get())
    if result == 1:
        messagebox.showinfo("result", "upgrade successful")
    else:
        messagebox.showinfo("result", "upgrade unsuccessful")


def upgrade_arrow_tower(event):
    result = DBM.upgrade_arrow_tower(upgradeAspect.get())
    if result == 1:
        messagebox.showinfo("result", "upgrade successful")
    else:
        messagebox.showinfo("result", "upgrade unsuccessful")


def upgrade_fire_tower(event):
    result = DBM.upgrade_fire_tower(upgradeAspect.get())
    if result == 1:
        messagebox.showinfo("result", "upgrade successful")
    else:
        messagebox.showinfo("result", "upgrade unsuccessful")


def upgrade_catapult_tower(event):
    result = DBM.upgrade_catapult_tower(upgradeAspect.get())
    if result == 1:
        messagebox.showinfo("result", "upgrade successful")
    else:
        messagebox.showinfo("result", "upgrade unsuccessful")


def discharge_wood(event):
    result = DBM.discharge_wood(upgradeAspect.get())
    if result == 1:
        messagebox.showinfo("result", "discharge successful")
    else:
        messagebox.showinfo("result", "discharge unsuccessful")


def discharge_food(event):
    result = DBM.discharge_food(upgradeAspect.get())
    if result == 1:
        messagebox.showinfo("result", "discharge successful")
    else:
        messagebox.showinfo("result", "discharge unsuccessful")


def discharge_stone(event):
    result = DBM.discharge_stone(upgradeAspect.get())
    if result == 1:
        messagebox.showinfo("result", "discharge successful")
    else:
        messagebox.showinfo("result", "discharge unsuccessful")


def discharge_iron(event):
    result = DBM.discharge_iron(upgradeAspect.get())
    if result == 1:
        messagebox.showinfo("result", "discharge successful")
    else:
        messagebox.showinfo("result", "discharge unsuccessful")


def run():
    startButton.bind("<Button-1>", start_new_game)
    backButton.bind("<Button-1>", show_main_page)
    exitButton.bind("<Button-1>", exit_game)

    ''' main menu '''
    statusButton.bind("<Button-1>", show_status)
    upgradeButton.bind("<Button-1>", upgrade_menu)
    attackButton.bind("<Button-1>", attack_menu)
    dischargeButton.bind("<Button-1>", discharge_menu)

    ''' upgrade menu '''
    upgradeWoodProducerButton.bind("<Button-1>", upgrade_wood_producer)
    upgradeFoodProducerButton.bind("<Button-1>", upgrade_food_producer)
    upgradeIronProducerButton.bind("<Button-1>", upgrade_iron_producer)
    upgradeStoneProducerButton.bind("<Button-1>", upgrade_stone_producer)

    upgradeWoodWarehouseButton.bind("<Button-1>", upgrade_wood_warehouse)
    upgradeFoodWarehouseButton.bind("<Button-1>", upgrade_food_warehouse)
    upgradeIronWarehouseButton.bind("<Button-1>", upgrade_iron_warehouse)
    upgradeStoneWarehouseButton.bind("<Button-1>", upgrade_stone_warehouse)

    upgradeCapitalButton.bind("<Button-1>", upgrade_capital)

    upgradeWhirlpoolButton.bind("<Button-1>", upgrade_whirlpool)
    upgradeTrenchButton.bind("<Button-1>", upgrade_trench)
    upgradeWallButton.bind("<Button-1>", upgrade_wall)

    upgradeArrowTowerButton.bind("<Button-1>", upgrade_arrow_tower)
    upgradeFireTowerButton.bind("<Button-1>", upgrade_fire_tower)
    upgradeCatapultTowerButton.bind("<Button-1>", upgrade_catapult_tower)
    upgradeArcherTowerButton.bind("<Button-1>", upgrade_archer_tower)

    dischargeWoodButton.bind("<Button-1>", discharge_wood)
    dischargeStoneButton.bind("<Button-1>", discharge_stone)
    dischargeIronButton.bind("<Button-1>", discharge_iron)
    dischargeFoodButton.bind("<Button-1>", discharge_food)

    startButton.pack()
    root.mainloop()


run()
