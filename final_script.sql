USE [master]
GO
/****** Object:  Database [XaravanDB]    Script Date: 1/11/2018 10:56:46 PM ******/
CREATE DATABASE [XaravanDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'XaravanDB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\XaravanDB.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'XaravanDB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\XaravanDB_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [XaravanDB] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [XaravanDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [XaravanDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [XaravanDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [XaravanDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [XaravanDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [XaravanDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [XaravanDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [XaravanDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [XaravanDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [XaravanDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [XaravanDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [XaravanDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [XaravanDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [XaravanDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [XaravanDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [XaravanDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [XaravanDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [XaravanDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [XaravanDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [XaravanDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [XaravanDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [XaravanDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [XaravanDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [XaravanDB] SET RECOVERY FULL 
GO
ALTER DATABASE [XaravanDB] SET  MULTI_USER 
GO
ALTER DATABASE [XaravanDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [XaravanDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [XaravanDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [XaravanDB] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [XaravanDB] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'XaravanDB', N'ON'
GO
ALTER DATABASE [XaravanDB] SET QUERY_STORE = OFF
GO
USE [XaravanDB]
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [XaravanDB]
GO
/****** Object:  User [sahand]    Script Date: 1/11/2018 10:56:46 PM ******/
CREATE USER [sahand] FOR LOGIN [sahand] WITH DEFAULT_SCHEMA=[sahand]
GO
/****** Object:  Schema [sahand]    Script Date: 1/11/2018 10:56:47 PM ******/
CREATE SCHEMA [sahand]
GO
/****** Object:  UserDefinedFunction [dbo].[find_upgradable_type]    Script Date: 1/11/2018 10:56:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[find_upgradable_type]
(	
	@upgradable_id int
)
RETURNS VARCHAR(255)
AS
BEGIN
	DECLARE @result_id int
    -- Insert statements for procedure here
	SELECT @result_id=p.id 
		FROM Producers p 
		WHERE p.upgradable_id = @upgradable_id;
	IF @result_id IS NOT NULL
	BEGIN
		RETURN 'producer';
	END
	SELECT @result_id=w.id 
		FROM Warehouses w 
		WHERE w.upgradable_id = @upgradable_id;
	IF @result_id IS NOT NULL
	BEGIN
		RETURN 'warehouse';
	END
	RETURN NULL;
END
GO
/****** Object:  UserDefinedFunction [dbo].[find_upgradable_type2]    Script Date: 1/11/2018 10:56:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[find_upgradable_type2]
(	
	@upgradable_id int
)
RETURNS VARCHAR(255)
AS
BEGIN
	DECLARE @result_id int
    -- Insert statements for procedure here
	SELECT @result_id=p.id 
		FROM Producers p 
		WHERE p.upgradable_id = @upgradable_id;
	IF @result_id IS NOT NULL
	BEGIN
		RETURN 'producer';
	END
	SELECT @result_id=w.id 
		FROM Warehouses w 
		WHERE w.upgradable_id = @upgradable_id;
	IF @result_id IS NOT NULL
	BEGIN
		RETURN 'warehouse';
	END
	RETURN NULL;
END
GO
/****** Object:  Table [dbo].[Producers]    Script Date: 1/11/2018 10:56:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Producers](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[stored] [int] NOT NULL,
	[upgradable_id] [int] NOT NULL,
	[capital_id] [int] NOT NULL,
	[resource_name] [varchar](255) NOT NULL,
 CONSTRAINT [PK_Producers] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  UserDefinedFunction [dbo].[get_all_producers]    Script Date: 1/11/2018 10:56:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[get_all_producers] (@capital_id INT)
RETURNS TABLE 
AS
RETURN 
(
	SELECT * FROM Producers p WHERE p.capital_id = @capital_id
)
GO
/****** Object:  Table [dbo].[Warehouses]    Script Date: 1/11/2018 10:56:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Warehouses](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[stored] [int] NOT NULL,
	[upgradable_id] [int] NOT NULL,
	[capital_id] [int] NOT NULL,
	[resource_name] [varchar](255) NOT NULL,
 CONSTRAINT [PK_Warehouses] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  UserDefinedFunction [dbo].[get_all_warehouses]    Script Date: 1/11/2018 10:56:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[get_all_warehouses] (@capital_id INT)
RETURNS TABLE 
AS
RETURN 
(
	SELECT * FROM Warehouses w WHERE w.capital_id = @capital_id
)
GO
/****** Object:  Table [dbo].[defensives]    Script Date: 1/11/2018 10:56:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[defensives](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[defensive_type] [varchar](255) NOT NULL,
	[capital_id] [int] NOT NULL,
	[upgradable_id] [int] NOT NULL,
 CONSTRAINT [PK_defensive_buildings] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  UserDefinedFunction [dbo].[get_all_defensives]    Script Date: 1/11/2018 10:56:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[get_all_defensives] (@capital_id INT)
RETURNS TABLE 
AS
RETURN 
(
	SELECT * FROM defensives d WHERE d.capital_id = @capital_id
)
GO
/****** Object:  Table [dbo].[offensive]    Script Date: 1/11/2018 10:56:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[offensive](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[offensive_type] [varchar](255) NOT NULL,
	[capital_id] [int] NOT NULL,
	[upgradable_id] [int] NOT NULL,
 CONSTRAINT [PK_offensive_buildings] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  UserDefinedFunction [dbo].[get_all_offensives]    Script Date: 1/11/2018 10:56:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[get_all_offensives] (@capital_id INT)
RETURNS TABLE 
AS
RETURN 
(
	SELECT * FROM offensive o WHERE o.capital_id = @capital_id
)
GO
/****** Object:  Table [dbo].[Capitals]    Script Date: 1/11/2018 10:56:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Capitals](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[turn_id] [int] NOT NULL,
 CONSTRAINT [PK_Capitals] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[defensive_levels]    Script Date: 1/11/2018 10:56:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[defensive_levels](
	[defensive_id] [int] NOT NULL,
	[level_id] [int] NOT NULL,
 CONSTRAINT [PK_defensive_building_levels] PRIMARY KEY CLUSTERED 
(
	[defensive_id] ASC,
	[level_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Levels]    Script Date: 1/11/2018 10:56:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Levels](
	[id] [int] NOT NULL,
	[speed] [int] NULL,
	[damage] [int] NULL,
	[capacity] [int] NULL,
	[resistance] [int] NOT NULL,
	[resource_required_id] [int] NOT NULL,
	[resource_required_value] [int] NOT NULL,
	[time_to_build] [int] NOT NULL,
	[turn_required_id] [int] NOT NULL,
	[level_type] [varchar](255) NOT NULL,
	[level_sub_type] [varchar](255) NOT NULL,
	[level_level] [int] NOT NULL,
 CONSTRAINT [PK_Levels] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[offensive_level]    Script Date: 1/11/2018 10:56:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[offensive_level](
	[offensive_id] [int] NOT NULL,
	[level_id] [int] NOT NULL,
 CONSTRAINT [PK_offensive_building_level] PRIMARY KEY CLUSTERED 
(
	[offensive_id] ASC,
	[level_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Producer_Level]    Script Date: 1/11/2018 10:56:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Producer_Level](
	[producer_id] [int] NOT NULL,
	[level_id] [int] NOT NULL,
 CONSTRAINT [PK_Producer_Level] PRIMARY KEY CLUSTERED 
(
	[producer_id] ASC,
	[level_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Resources]    Script Date: 1/11/2018 10:56:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Resources](
	[id] [int] NOT NULL,
	[resource_name] [varchar](255) NOT NULL,
 CONSTRAINT [PK_Resources] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[test]    Script Date: 1/11/2018 10:56:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[test](
	[id] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Turns]    Script Date: 1/11/2018 10:56:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Turns](
	[id] [int] NOT NULL,
	[name] [varchar](255) NOT NULL,
	[resource_require_id] [int] NOT NULL,
	[resource_required_value] [int] NOT NULL,
 CONSTRAINT [PK_Turns] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Upgradable]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Upgradable](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[upgradable_type] [varchar](255) NOT NULL,
 CONSTRAINT [PK_Upgradable] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Warehouse_Level]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Warehouse_Level](
	[level_id] [int] NOT NULL,
	[warehouse_id] [int] NOT NULL,
 CONSTRAINT [PK_Warehouse_Level] PRIMARY KEY CLUSTERED 
(
	[level_id] ASC,
	[warehouse_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Workers]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Workers](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[start_time] [timestamp] NULL,
	[available] [int] NOT NULL,
	[upgradable_id] [int] NULL,
	[capital_id] [int] NOT NULL,
 CONSTRAINT [PK_Workers] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Works]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Works](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[upgradable_id] [int] NOT NULL,
	[worker_id] [int] NOT NULL,
	[started_time] [datetime] NOT NULL,
	[time_to_build] [int] NOT NULL,
	[level_id] [int] NOT NULL,
 CONSTRAINT [PK_Works] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Levels] ([id], [speed], [damage], [capacity], [resistance], [resource_required_id], [resource_required_value], [time_to_build], [turn_required_id], [level_type], [level_sub_type], [level_level]) VALUES (1, 20, NULL, 0, 0, 1, 0, 0, 1, N'wood_producer', N'speed', 1)
INSERT [dbo].[Levels] ([id], [speed], [damage], [capacity], [resistance], [resource_required_id], [resource_required_value], [time_to_build], [turn_required_id], [level_type], [level_sub_type], [level_level]) VALUES (2, 0, NULL, 100, 0, 1, 0, 0, 1, N'wood_producer', N'capacity', 1)
INSERT [dbo].[Levels] ([id], [speed], [damage], [capacity], [resistance], [resource_required_id], [resource_required_value], [time_to_build], [turn_required_id], [level_type], [level_sub_type], [level_level]) VALUES (3, 0, NULL, 0, 100, 1, 0, 0, 1, N'wood_producer', N'resistance', 1)
INSERT [dbo].[Levels] ([id], [speed], [damage], [capacity], [resistance], [resource_required_id], [resource_required_value], [time_to_build], [turn_required_id], [level_type], [level_sub_type], [level_level]) VALUES (4, 20, NULL, 0, 0, 2, 0, 0, 1, N'food_producer', N'speed', 1)
INSERT [dbo].[Levels] ([id], [speed], [damage], [capacity], [resistance], [resource_required_id], [resource_required_value], [time_to_build], [turn_required_id], [level_type], [level_sub_type], [level_level]) VALUES (5, 0, NULL, 100, 0, 2, 0, 0, 1, N'food_producer', N'capacity', 1)
INSERT [dbo].[Levels] ([id], [speed], [damage], [capacity], [resistance], [resource_required_id], [resource_required_value], [time_to_build], [turn_required_id], [level_type], [level_sub_type], [level_level]) VALUES (6, 0, NULL, 0, 100, 2, 0, 0, 1, N'food_producer', N'resistance', 1)
INSERT [dbo].[Levels] ([id], [speed], [damage], [capacity], [resistance], [resource_required_id], [resource_required_value], [time_to_build], [turn_required_id], [level_type], [level_sub_type], [level_level]) VALUES (7, 20, NULL, 0, 0, 3, 0, 0, 1, N'stone_producer', N'speed', 1)
INSERT [dbo].[Levels] ([id], [speed], [damage], [capacity], [resistance], [resource_required_id], [resource_required_value], [time_to_build], [turn_required_id], [level_type], [level_sub_type], [level_level]) VALUES (8, 0, NULL, 100, 0, 3, 0, 0, 1, N'stone_producer', N'capacity', 1)
INSERT [dbo].[Levels] ([id], [speed], [damage], [capacity], [resistance], [resource_required_id], [resource_required_value], [time_to_build], [turn_required_id], [level_type], [level_sub_type], [level_level]) VALUES (9, 0, NULL, 0, 100, 3, 0, 0, 1, N'stone_producer', N'resistance', 1)
INSERT [dbo].[Levels] ([id], [speed], [damage], [capacity], [resistance], [resource_required_id], [resource_required_value], [time_to_build], [turn_required_id], [level_type], [level_sub_type], [level_level]) VALUES (10, 20, NULL, 0, 0, 4, 0, 0, 1, N'iron_producer', N'speed', 1)
INSERT [dbo].[Levels] ([id], [speed], [damage], [capacity], [resistance], [resource_required_id], [resource_required_value], [time_to_build], [turn_required_id], [level_type], [level_sub_type], [level_level]) VALUES (11, 0, NULL, 100, 0, 4, 0, 0, 1, N'iron_producer', N'capacity', 1)
INSERT [dbo].[Levels] ([id], [speed], [damage], [capacity], [resistance], [resource_required_id], [resource_required_value], [time_to_build], [turn_required_id], [level_type], [level_sub_type], [level_level]) VALUES (12, 0, NULL, 0, 100, 4, 0, 0, 1, N'iron_producer', N'resistance', 1)
INSERT [dbo].[Levels] ([id], [speed], [damage], [capacity], [resistance], [resource_required_id], [resource_required_value], [time_to_build], [turn_required_id], [level_type], [level_sub_type], [level_level]) VALUES (13, NULL, NULL, 1000, 0, 1, 0, 0, 1, N'wood_warehouse', N'capacity', 1)
INSERT [dbo].[Levels] ([id], [speed], [damage], [capacity], [resistance], [resource_required_id], [resource_required_value], [time_to_build], [turn_required_id], [level_type], [level_sub_type], [level_level]) VALUES (14, NULL, NULL, 0, 1000, 1, 0, 0, 1, N'wood_warehouse', N'resistance', 1)
INSERT [dbo].[Levels] ([id], [speed], [damage], [capacity], [resistance], [resource_required_id], [resource_required_value], [time_to_build], [turn_required_id], [level_type], [level_sub_type], [level_level]) VALUES (15, NULL, NULL, 1000, 0, 2, 0, 0, 1, N'food_warehouse', N'capacity', 1)
INSERT [dbo].[Levels] ([id], [speed], [damage], [capacity], [resistance], [resource_required_id], [resource_required_value], [time_to_build], [turn_required_id], [level_type], [level_sub_type], [level_level]) VALUES (16, NULL, NULL, 0, 1000, 2, 0, 0, 1, N'food_warehouse', N'resistance', 1)
INSERT [dbo].[Levels] ([id], [speed], [damage], [capacity], [resistance], [resource_required_id], [resource_required_value], [time_to_build], [turn_required_id], [level_type], [level_sub_type], [level_level]) VALUES (17, NULL, NULL, 1000, 0, 3, 0, 0, 1, N'stone_warehouse', N'capacity', 1)
INSERT [dbo].[Levels] ([id], [speed], [damage], [capacity], [resistance], [resource_required_id], [resource_required_value], [time_to_build], [turn_required_id], [level_type], [level_sub_type], [level_level]) VALUES (18, NULL, NULL, 0, 1000, 3, 0, 0, 1, N'stone_warehouse', N'resistance', 1)
INSERT [dbo].[Levels] ([id], [speed], [damage], [capacity], [resistance], [resource_required_id], [resource_required_value], [time_to_build], [turn_required_id], [level_type], [level_sub_type], [level_level]) VALUES (19, NULL, NULL, 1000, 0, 4, 0, 0, 1, N'iron_warehouse', N'capacity', 1)
INSERT [dbo].[Levels] ([id], [speed], [damage], [capacity], [resistance], [resource_required_id], [resource_required_value], [time_to_build], [turn_required_id], [level_type], [level_sub_type], [level_level]) VALUES (20, NULL, NULL, 0, 1000, 4, 0, 0, 1, N'iron_warehouse', N'resistance', 1)
INSERT [dbo].[Levels] ([id], [speed], [damage], [capacity], [resistance], [resource_required_id], [resource_required_value], [time_to_build], [turn_required_id], [level_type], [level_sub_type], [level_level]) VALUES (21, NULL, 100, NULL, 0, 1, 0, 0, 1, N'archer_tower', N'damage', 1)
INSERT [dbo].[Levels] ([id], [speed], [damage], [capacity], [resistance], [resource_required_id], [resource_required_value], [time_to_build], [turn_required_id], [level_type], [level_sub_type], [level_level]) VALUES (22, NULL, 100, NULL, 0, 2, 0, 0, 1, N'fire_tower', N'damage', 1)
INSERT [dbo].[Levels] ([id], [speed], [damage], [capacity], [resistance], [resource_required_id], [resource_required_value], [time_to_build], [turn_required_id], [level_type], [level_sub_type], [level_level]) VALUES (23, NULL, 100, NULL, 0, 3, 0, 0, 1, N'arrow_tower', N'damage', 1)
INSERT [dbo].[Levels] ([id], [speed], [damage], [capacity], [resistance], [resource_required_id], [resource_required_value], [time_to_build], [turn_required_id], [level_type], [level_sub_type], [level_level]) VALUES (24, NULL, 100, NULL, 0, 4, 0, 0, 1, N'catapult_tower', N'damage', 1)
INSERT [dbo].[Levels] ([id], [speed], [damage], [capacity], [resistance], [resource_required_id], [resource_required_value], [time_to_build], [turn_required_id], [level_type], [level_sub_type], [level_level]) VALUES (25, NULL, NULL, NULL, 100, 1, 0, 0, 1, N'trench', N'resistance', 1)
INSERT [dbo].[Levels] ([id], [speed], [damage], [capacity], [resistance], [resource_required_id], [resource_required_value], [time_to_build], [turn_required_id], [level_type], [level_sub_type], [level_level]) VALUES (26, NULL, NULL, NULL, 100, 2, 0, 0, 1, N'whirlpool', N'resistance', 1)
INSERT [dbo].[Levels] ([id], [speed], [damage], [capacity], [resistance], [resource_required_id], [resource_required_value], [time_to_build], [turn_required_id], [level_type], [level_sub_type], [level_level]) VALUES (27, NULL, NULL, NULL, 100, 3, 0, 0, 1, N'wall', N'resistance', 1)
INSERT [dbo].[Levels] ([id], [speed], [damage], [capacity], [resistance], [resource_required_id], [resource_required_value], [time_to_build], [turn_required_id], [level_type], [level_sub_type], [level_level]) VALUES (28, 40, NULL, 0, 0, 1, 0, 0, 2, N'wood_producer', N'speed', 2)
INSERT [dbo].[Levels] ([id], [speed], [damage], [capacity], [resistance], [resource_required_id], [resource_required_value], [time_to_build], [turn_required_id], [level_type], [level_sub_type], [level_level]) VALUES (29, 0, NULL, 200, 0, 1, 100, 1, 2, N'wood_producer', N'capacity', 2)
INSERT [dbo].[Levels] ([id], [speed], [damage], [capacity], [resistance], [resource_required_id], [resource_required_value], [time_to_build], [turn_required_id], [level_type], [level_sub_type], [level_level]) VALUES (30, 0, NULL, 0, 200, 1, 100, 1, 2, N'wood_producer', N'resistance', 2)
INSERT [dbo].[Levels] ([id], [speed], [damage], [capacity], [resistance], [resource_required_id], [resource_required_value], [time_to_build], [turn_required_id], [level_type], [level_sub_type], [level_level]) VALUES (31, 40, NULL, 0, 0, 2, 100, 1, 2, N'food_producer', N'speed', 2)
INSERT [dbo].[Levels] ([id], [speed], [damage], [capacity], [resistance], [resource_required_id], [resource_required_value], [time_to_build], [turn_required_id], [level_type], [level_sub_type], [level_level]) VALUES (32, 0, NULL, 200, 0, 2, 100, 1, 2, N'food_producer', N'capacity', 2)
INSERT [dbo].[Levels] ([id], [speed], [damage], [capacity], [resistance], [resource_required_id], [resource_required_value], [time_to_build], [turn_required_id], [level_type], [level_sub_type], [level_level]) VALUES (33, 0, NULL, 0, 200, 2, 100, 1, 2, N'food_producer', N'resistance', 2)
INSERT [dbo].[Levels] ([id], [speed], [damage], [capacity], [resistance], [resource_required_id], [resource_required_value], [time_to_build], [turn_required_id], [level_type], [level_sub_type], [level_level]) VALUES (34, 40, NULL, 0, 0, 3, 100, 1, 2, N'stone_producer', N'speed', 2)
INSERT [dbo].[Levels] ([id], [speed], [damage], [capacity], [resistance], [resource_required_id], [resource_required_value], [time_to_build], [turn_required_id], [level_type], [level_sub_type], [level_level]) VALUES (35, 0, NULL, 200, 0, 3, 100, 1, 2, N'stone_producer', N'capacity', 2)
INSERT [dbo].[Levels] ([id], [speed], [damage], [capacity], [resistance], [resource_required_id], [resource_required_value], [time_to_build], [turn_required_id], [level_type], [level_sub_type], [level_level]) VALUES (36, 0, NULL, 0, 200, 3, 100, 1, 2, N'stone_producer', N'resistance', 2)
INSERT [dbo].[Levels] ([id], [speed], [damage], [capacity], [resistance], [resource_required_id], [resource_required_value], [time_to_build], [turn_required_id], [level_type], [level_sub_type], [level_level]) VALUES (37, 40, NULL, 0, 0, 4, 100, 1, 2, N'iron_producer', N'speed', 2)
INSERT [dbo].[Levels] ([id], [speed], [damage], [capacity], [resistance], [resource_required_id], [resource_required_value], [time_to_build], [turn_required_id], [level_type], [level_sub_type], [level_level]) VALUES (38, 0, NULL, 200, 0, 4, 100, 1, 2, N'iron_producer', N'capacity', 2)
INSERT [dbo].[Levels] ([id], [speed], [damage], [capacity], [resistance], [resource_required_id], [resource_required_value], [time_to_build], [turn_required_id], [level_type], [level_sub_type], [level_level]) VALUES (39, 0, NULL, 0, 200, 4, 100, 1, 2, N'iron_producer', N'resistance', 2)
INSERT [dbo].[Levels] ([id], [speed], [damage], [capacity], [resistance], [resource_required_id], [resource_required_value], [time_to_build], [turn_required_id], [level_type], [level_sub_type], [level_level]) VALUES (40, NULL, NULL, 2000, 0, 1, 100, 1, 2, N'wood_warehouse', N'capacity', 2)
INSERT [dbo].[Levels] ([id], [speed], [damage], [capacity], [resistance], [resource_required_id], [resource_required_value], [time_to_build], [turn_required_id], [level_type], [level_sub_type], [level_level]) VALUES (41, NULL, NULL, NULL, 2000, 1, 100, 1, 2, N'wood_warehouse', N'resistance', 2)
INSERT [dbo].[Levels] ([id], [speed], [damage], [capacity], [resistance], [resource_required_id], [resource_required_value], [time_to_build], [turn_required_id], [level_type], [level_sub_type], [level_level]) VALUES (42, NULL, NULL, 2000, 0, 2, 100, 1, 2, N'food_warehouse', N'capacity', 2)
INSERT [dbo].[Levels] ([id], [speed], [damage], [capacity], [resistance], [resource_required_id], [resource_required_value], [time_to_build], [turn_required_id], [level_type], [level_sub_type], [level_level]) VALUES (43, NULL, NULL, 0, 2000, 2, 100, 1, 2, N'food_warehouse', N'resistance', 2)
INSERT [dbo].[Levels] ([id], [speed], [damage], [capacity], [resistance], [resource_required_id], [resource_required_value], [time_to_build], [turn_required_id], [level_type], [level_sub_type], [level_level]) VALUES (44, NULL, NULL, 2000, 0, 3, 100, 1, 2, N'stone_warehouse', N'capacity', 2)
INSERT [dbo].[Levels] ([id], [speed], [damage], [capacity], [resistance], [resource_required_id], [resource_required_value], [time_to_build], [turn_required_id], [level_type], [level_sub_type], [level_level]) VALUES (45, NULL, NULL, 0, 2000, 3, 100, 1, 2, N'stone_warehouse', N'resistance', 2)
INSERT [dbo].[Levels] ([id], [speed], [damage], [capacity], [resistance], [resource_required_id], [resource_required_value], [time_to_build], [turn_required_id], [level_type], [level_sub_type], [level_level]) VALUES (46, NULL, NULL, 2000, 0, 4, 100, 1, 2, N'iron_warehouse', N'capacity', 2)
INSERT [dbo].[Levels] ([id], [speed], [damage], [capacity], [resistance], [resource_required_id], [resource_required_value], [time_to_build], [turn_required_id], [level_type], [level_sub_type], [level_level]) VALUES (47, NULL, NULL, 0, 2000, 4, 100, 1, 2, N'iron_warehouse', N'resistance', 2)
INSERT [dbo].[Levels] ([id], [speed], [damage], [capacity], [resistance], [resource_required_id], [resource_required_value], [time_to_build], [turn_required_id], [level_type], [level_sub_type], [level_level]) VALUES (48, NULL, 200, NULL, 0, 1, 100, 1, 2, N'archer_tower', N'damage', 2)
INSERT [dbo].[Levels] ([id], [speed], [damage], [capacity], [resistance], [resource_required_id], [resource_required_value], [time_to_build], [turn_required_id], [level_type], [level_sub_type], [level_level]) VALUES (49, NULL, 200, NULL, 0, 2, 100, 1, 2, N'fire_tower', N'damage', 2)
INSERT [dbo].[Levels] ([id], [speed], [damage], [capacity], [resistance], [resource_required_id], [resource_required_value], [time_to_build], [turn_required_id], [level_type], [level_sub_type], [level_level]) VALUES (50, NULL, 200, NULL, 0, 3, 100, 1, 2, N'arrow_tower', N'damage', 2)
INSERT [dbo].[Levels] ([id], [speed], [damage], [capacity], [resistance], [resource_required_id], [resource_required_value], [time_to_build], [turn_required_id], [level_type], [level_sub_type], [level_level]) VALUES (51, NULL, 200, NULL, 0, 4, 100, 1, 2, N'catapult_tower', N'damage', 2)
INSERT [dbo].[Levels] ([id], [speed], [damage], [capacity], [resistance], [resource_required_id], [resource_required_value], [time_to_build], [turn_required_id], [level_type], [level_sub_type], [level_level]) VALUES (52, NULL, NULL, NULL, 200, 1, 100, 1, 2, N'trench', N'resistance', 2)
INSERT [dbo].[Levels] ([id], [speed], [damage], [capacity], [resistance], [resource_required_id], [resource_required_value], [time_to_build], [turn_required_id], [level_type], [level_sub_type], [level_level]) VALUES (53, NULL, NULL, NULL, 200, 2, 100, 1, 2, N'whirlpool', N'resistance', 2)
INSERT [dbo].[Levels] ([id], [speed], [damage], [capacity], [resistance], [resource_required_id], [resource_required_value], [time_to_build], [turn_required_id], [level_type], [level_sub_type], [level_level]) VALUES (54, NULL, NULL, NULL, 200, 3, 100, 1, 2, N'wall', N'resistance', 2)
INSERT [dbo].[Resources] ([id], [resource_name]) VALUES (1, N'wood')
INSERT [dbo].[Resources] ([id], [resource_name]) VALUES (2, N'food')
INSERT [dbo].[Resources] ([id], [resource_name]) VALUES (3, N'stone')
INSERT [dbo].[Resources] ([id], [resource_name]) VALUES (4, N'iron')
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
GO
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
GO
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[test] ([id]) VALUES (1)
INSERT [dbo].[Turns] ([id], [name], [resource_require_id], [resource_required_value]) VALUES (1, N'turn1', 1, 0)
INSERT [dbo].[Turns] ([id], [name], [resource_require_id], [resource_required_value]) VALUES (2, N'turn2', 1, 1000)
ALTER TABLE [dbo].[Workers] ADD  CONSTRAINT [DF_Workers_busy]  DEFAULT ((0)) FOR [available]
GO
ALTER TABLE [dbo].[Capitals]  WITH CHECK ADD  CONSTRAINT [FK_Capitals_Turns] FOREIGN KEY([turn_id])
REFERENCES [dbo].[Turns] ([id])
GO
ALTER TABLE [dbo].[Capitals] CHECK CONSTRAINT [FK_Capitals_Turns]
GO
ALTER TABLE [dbo].[defensive_levels]  WITH CHECK ADD  CONSTRAINT [FK_defensive_building_levels_defensive_buildings] FOREIGN KEY([defensive_id])
REFERENCES [dbo].[defensives] ([id])
GO
ALTER TABLE [dbo].[defensive_levels] CHECK CONSTRAINT [FK_defensive_building_levels_defensive_buildings]
GO
ALTER TABLE [dbo].[defensive_levels]  WITH CHECK ADD  CONSTRAINT [FK_defensive_building_levels_Levels] FOREIGN KEY([level_id])
REFERENCES [dbo].[Levels] ([id])
GO
ALTER TABLE [dbo].[defensive_levels] CHECK CONSTRAINT [FK_defensive_building_levels_Levels]
GO
ALTER TABLE [dbo].[defensives]  WITH CHECK ADD  CONSTRAINT [FK_defensive_buildings_Capitals] FOREIGN KEY([capital_id])
REFERENCES [dbo].[Capitals] ([id])
GO
ALTER TABLE [dbo].[defensives] CHECK CONSTRAINT [FK_defensive_buildings_Capitals]
GO
ALTER TABLE [dbo].[defensives]  WITH CHECK ADD  CONSTRAINT [FK_defensive_buildings_Upgradable] FOREIGN KEY([upgradable_id])
REFERENCES [dbo].[Upgradable] ([id])
GO
ALTER TABLE [dbo].[defensives] CHECK CONSTRAINT [FK_defensive_buildings_Upgradable]
GO
ALTER TABLE [dbo].[Levels]  WITH CHECK ADD  CONSTRAINT [FK_Levels_Turns] FOREIGN KEY([turn_required_id])
REFERENCES [dbo].[Turns] ([id])
GO
ALTER TABLE [dbo].[Levels] CHECK CONSTRAINT [FK_Levels_Turns]
GO
ALTER TABLE [dbo].[offensive]  WITH CHECK ADD  CONSTRAINT [FK_offensive_buildings_Capitals] FOREIGN KEY([capital_id])
REFERENCES [dbo].[Capitals] ([id])
GO
ALTER TABLE [dbo].[offensive] CHECK CONSTRAINT [FK_offensive_buildings_Capitals]
GO
ALTER TABLE [dbo].[offensive]  WITH CHECK ADD  CONSTRAINT [FK_offensive_buildings_offensive_buildings] FOREIGN KEY([id])
REFERENCES [dbo].[offensive] ([id])
GO
ALTER TABLE [dbo].[offensive] CHECK CONSTRAINT [FK_offensive_buildings_offensive_buildings]
GO
ALTER TABLE [dbo].[offensive]  WITH CHECK ADD  CONSTRAINT [FK_offensive_buildings_Upgradable] FOREIGN KEY([upgradable_id])
REFERENCES [dbo].[Upgradable] ([id])
GO
ALTER TABLE [dbo].[offensive] CHECK CONSTRAINT [FK_offensive_buildings_Upgradable]
GO
ALTER TABLE [dbo].[offensive_level]  WITH CHECK ADD  CONSTRAINT [FK_offensive_building_level_Levels] FOREIGN KEY([level_id])
REFERENCES [dbo].[Levels] ([id])
GO
ALTER TABLE [dbo].[offensive_level] CHECK CONSTRAINT [FK_offensive_building_level_Levels]
GO
ALTER TABLE [dbo].[offensive_level]  WITH CHECK ADD  CONSTRAINT [FK_offensive_building_level_offensive_buildings] FOREIGN KEY([offensive_id])
REFERENCES [dbo].[offensive] ([id])
GO
ALTER TABLE [dbo].[offensive_level] CHECK CONSTRAINT [FK_offensive_building_level_offensive_buildings]
GO
ALTER TABLE [dbo].[Producer_Level]  WITH CHECK ADD  CONSTRAINT [FK_Producer_Level_Levels] FOREIGN KEY([level_id])
REFERENCES [dbo].[Levels] ([id])
GO
ALTER TABLE [dbo].[Producer_Level] CHECK CONSTRAINT [FK_Producer_Level_Levels]
GO
ALTER TABLE [dbo].[Producer_Level]  WITH CHECK ADD  CONSTRAINT [FK_Producer_Level_Producers] FOREIGN KEY([producer_id])
REFERENCES [dbo].[Producers] ([id])
GO
ALTER TABLE [dbo].[Producer_Level] CHECK CONSTRAINT [FK_Producer_Level_Producers]
GO
ALTER TABLE [dbo].[Producers]  WITH CHECK ADD  CONSTRAINT [FK_Producers_Capitals] FOREIGN KEY([capital_id])
REFERENCES [dbo].[Capitals] ([id])
GO
ALTER TABLE [dbo].[Producers] CHECK CONSTRAINT [FK_Producers_Capitals]
GO
ALTER TABLE [dbo].[Producers]  WITH CHECK ADD  CONSTRAINT [FK_Producers_Upgradable] FOREIGN KEY([upgradable_id])
REFERENCES [dbo].[Upgradable] ([id])
GO
ALTER TABLE [dbo].[Producers] CHECK CONSTRAINT [FK_Producers_Upgradable]
GO
ALTER TABLE [dbo].[Turns]  WITH CHECK ADD  CONSTRAINT [FK_Turns_Resources] FOREIGN KEY([resource_require_id])
REFERENCES [dbo].[Resources] ([id])
GO
ALTER TABLE [dbo].[Turns] CHECK CONSTRAINT [FK_Turns_Resources]
GO
ALTER TABLE [dbo].[Warehouse_Level]  WITH CHECK ADD  CONSTRAINT [FK_Warehouse_Level_Levels] FOREIGN KEY([level_id])
REFERENCES [dbo].[Levels] ([id])
GO
ALTER TABLE [dbo].[Warehouse_Level] CHECK CONSTRAINT [FK_Warehouse_Level_Levels]
GO
ALTER TABLE [dbo].[Warehouse_Level]  WITH CHECK ADD  CONSTRAINT [FK_Warehouse_Level_Warehouses] FOREIGN KEY([warehouse_id])
REFERENCES [dbo].[Warehouses] ([id])
GO
ALTER TABLE [dbo].[Warehouse_Level] CHECK CONSTRAINT [FK_Warehouse_Level_Warehouses]
GO
ALTER TABLE [dbo].[Warehouses]  WITH CHECK ADD  CONSTRAINT [FK_Warehouses_Capitals] FOREIGN KEY([capital_id])
REFERENCES [dbo].[Capitals] ([id])
GO
ALTER TABLE [dbo].[Warehouses] CHECK CONSTRAINT [FK_Warehouses_Capitals]
GO
ALTER TABLE [dbo].[Warehouses]  WITH CHECK ADD  CONSTRAINT [FK_Warehouses_Upgradable] FOREIGN KEY([upgradable_id])
REFERENCES [dbo].[Upgradable] ([id])
GO
ALTER TABLE [dbo].[Warehouses] CHECK CONSTRAINT [FK_Warehouses_Upgradable]
GO
ALTER TABLE [dbo].[Workers]  WITH CHECK ADD  CONSTRAINT [FK_Workers_Capitals] FOREIGN KEY([capital_id])
REFERENCES [dbo].[Capitals] ([id])
GO
ALTER TABLE [dbo].[Workers] CHECK CONSTRAINT [FK_Workers_Capitals]
GO
ALTER TABLE [dbo].[Workers]  WITH CHECK ADD  CONSTRAINT [FK_Workers_Upgradable] FOREIGN KEY([upgradable_id])
REFERENCES [dbo].[Upgradable] ([id])
GO
ALTER TABLE [dbo].[Workers] CHECK CONSTRAINT [FK_Workers_Upgradable]
GO
ALTER TABLE [dbo].[Works]  WITH CHECK ADD  CONSTRAINT [FK_Works_Upgradable] FOREIGN KEY([upgradable_id])
REFERENCES [dbo].[Upgradable] ([id])
GO
ALTER TABLE [dbo].[Works] CHECK CONSTRAINT [FK_Works_Upgradable]
GO
ALTER TABLE [dbo].[Works]  WITH CHECK ADD  CONSTRAINT [FK_Works_Workers] FOREIGN KEY([worker_id])
REFERENCES [dbo].[Workers] ([id])
GO
ALTER TABLE [dbo].[Works] CHECK CONSTRAINT [FK_Works_Workers]
GO
/****** Object:  StoredProcedure [dbo].[add_level]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[add_level](@upgradable_id INT, @level_id INT)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @upgradable_type VARCHAR(255);

	SELECT @upgradable_type = u.upgradable_type FROM Upgradable u
		WHERE u.id = @upgradable_id;

	IF @upgradable_type = 'producer'
	BEGIN
		DECLARE @producer_id INT;
		SELECT @producer_id = p.id FROM Producers p 
			WHERE p.upgradable_id = @upgradable_id;
		
		INSERT INTO Producer_Level (producer_id, level_id)
			VALUES (@producer_id, @level_id);
	END

	ELSE IF @upgradable_type = 'warehouse'
	BEGIN
		DECLARE @warehouse_id INT;
		SELECT @warehouse_id = w.id FROM Warehouses w 
			WHERE w.upgradable_id = @upgradable_id;
		
		INSERT INTO Warehouse_Level(warehouse_id, level_id)
			VALUES (@warehouse_id, @level_id);
	END

	ELSE IF @upgradable_type = 'defensive'
	BEGIN
		DECLARE @defensive_id INT;
		SELECT @defensive_id = d.id FROM defensives d 
			WHERE d.upgradable_id = @upgradable_id;
		
		INSERT INTO defensive_levels(defensive_id, level_id)
			VALUES (@defensive_id, @level_id);
	END

	ELSE IF @upgradable_type = 'offensive'
	BEGIN
		DECLARE @offensive_id INT;
		SELECT @offensive_id = o.id FROM offensive o
			WHERE o.upgradable_id = @upgradable_id;
		
		INSERT INTO offensive_level(offensive_id, level_id)
			VALUES (@offensive_id, @level_id);
	END
END
GO
/****** Object:  StoredProcedure [dbo].[calculate_upgradable_level]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[calculate_upgradable_level]
	@upgradable_id INT,
	@level REAL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @upgradable_type VARCHAR(255);
	--DECLARE @level FLOAT;

	SELECT @upgradable_type = u.upgradable_type 
		FROM Upgradable u Where u.id = @upgradable_id;

	IF @upgradable_type = 'producer'
	BEGIN
		SELECT @level=0.3*CAST(COUNT(*) AS FLOAT) FROM Producers p, Producer_Level pl, Levels l
			WHERE p.upgradable_id = @upgradable_id AND
				p.id = pl.producer_id AND
				pl.level_id = l.id;
			
	END
	ELSE IF @upgradable_type = 'warehouse'
	BEGIN
		SELECT @level=0.5*CAST(COUNT(*) AS FLOAT) FROM Warehouses w, Warehouse_Level wl, Levels l
			WHERE w.upgradable_id = @upgradable_id AND
				w.id = wl.warehouse_id AND
				wl.level_id = l.id;
	END
	RETURN @level;
END
GO
/****** Object:  StoredProcedure [dbo].[check_turn]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[check_turn] (@level_id INT, @turn_id INT)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @turn_required_id INT;
	SELECT @turn_required_id = l.turn_required_id FROM Levels l
		WHERE l.id = @level_id;

	IF @turn_id >= @turn_required_id	BEGIN
		RETURN 1;
	END 
	RETURN 0;
END
GO
/****** Object:  StoredProcedure [dbo].[check_works_done]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[check_works_done]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO test (id) VALUES (1)
END
GO
/****** Object:  StoredProcedure [dbo].[create_capital]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[create_capital] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @capital_id INT;

	INSERT INTO Capitals (turn_id) VALUES (1)
	EXEC @capital_id = get_last_capital;

	RETURN @capital_id;

END
GO
/****** Object:  StoredProcedure [dbo].[create_defensive]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[create_defensive](
	@defensive_type VARCHAR(255), 
	@capital_id INT
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @upgradable_id INT;
	DECLARE @defensive_id INT;

	DECLARE @first_damage_level_id INT;

	EXEC @first_damage_level_id = get_first_level_id @defensive_type, 'resistance'

	INSERT INTO Upgradable (upgradable_type) 
		VALUES('defensive')
	EXEC @upgradable_id = get_last_upgradable_id

	INSERT INTO defensives(capital_id, defensive_type, upgradable_id) 
		VALUES(@capital_id, @defensive_type, @upgradable_id);
	EXEC @defensive_id = get_last_defensive_id;

	INSERT INTO defensive_levels(defensive_id, level_id)
		VALUES (@defensive_id, @first_damage_level_id)

END
GO
/****** Object:  StoredProcedure [dbo].[create_offensive]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[create_offensive](
	@offensive_type VARCHAR(255), 
	@capital_id INT
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @upgradable_id INT;
	DECLARE @offensive_id INT;

	DECLARE @first_damage_level_id INT;

	EXEC @first_damage_level_id = get_first_level_id @offensive_type, 'damage'

	INSERT INTO Upgradable (upgradable_type) 
		VALUES('offensive')
	EXEC @upgradable_id = get_last_upgradable_id

	INSERT INTO offensive(capital_id, offensive_type, upgradable_id) 
		VALUES(@capital_id, @offensive_type, @upgradable_id);
	EXEC @offensive_id = get_last_offensive_id;

	INSERT INTO offensive_level(offensive_id, level_id)
		VALUES (@offensive_id, @first_damage_level_id)

END
GO
/****** Object:  StoredProcedure [dbo].[create_producer]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[create_producer](
	@producer_type VARCHAR(255), 
	@capital_id INT
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @worker_id INT;
	DECLARE @time_to_build INT;
	DECLARE @is_worker_available INT;
	DECLARE @upgradable_id INT;
	DECLARE @producer_id INT;

	DECLARE @first_speed_level_id INT;
	DECLARE @first_capacity_level_id INT;
	DECLARE @first_resistance_level_id INT;

	DECLARE @resource_name VARCHAR(255);
	IF @producer_type = 'wood_producer'
		SET @resource_name = 'wood';
	ELSE IF @producer_type = 'food_producer'
		SET @resource_name = 'food';
	ELSE IF @producer_type = 'stone_producer'
		SET @resource_name = 'stone'
	ELSE
		SET @resource_name = 'iron'


	EXEC @worker_id = get_capital_worker @capital_id;
	EXEC @is_worker_available = is_worker_available @worker_id;

	EXEC @first_speed_level_id = get_first_level_id @producer_type, 'speed'
	EXEC @first_capacity_level_id = get_first_level_id @producer_type, 'capacity'
	EXEC @first_resistance_level_id = get_first_level_id @producer_type, 'resistance'

	EXEC @time_to_build = get_level_during_time @first_speed_level_id

	INSERT INTO Upgradable (upgradable_type) 
		VALUES('producer')
	EXEC @upgradable_id = get_last_upgradable_id

	INSERT INTO Producers (stored, resource_name, capital_id, upgradable_id) 
		VALUES(0, @resource_name, @capital_id, @upgradable_id)
	EXEC @producer_id = get_last_producer_id;

	INSERT INTO Producer_Level (producer_id, level_id)
		VALUES (@producer_id, @first_speed_level_id)
	INSERT INTO Producer_Level (producer_id, level_id)
		VALUES (@producer_id, @first_capacity_level_id)
	INSERT INTO Producer_Level (producer_id, level_id)
		VALUES (@producer_id, @first_resistance_level_id)
	/*
    IF @is_worker_available = 1
	BEGIN
		EXEC set_worker_availability @worker_id, 0
		INSERT INTO Works (upgradable_id, worker_id, time_to_build, level_id, started_time) 
			VALUES (@upgradable_id + 1, @worker_id, @time_to_build, @first_speed_level_id, GETDATE())
		INSERT INTO Works (upgradable_id, worker_id, time_to_build, level_id, started_time) 
			VALUES (@upgradable_id + 1, @worker_id, @time_to_build, @first_capacity_level_id, GETDATE())
		INSERT INTO Works (upgradable_id, worker_id, time_to_build, level_id, started_time) 
			VALUES (@upgradable_id + 1, @worker_id, @time_to_build, @first_resistance_level_id, GETDATE())
	END
	*/
	
END
GO
/****** Object:  StoredProcedure [dbo].[create_warehouse]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[create_warehouse](
	@warehouse_type VARCHAR(255), 
	@capital_id INT
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @worker_id INT;
	DECLARE @is_worker_available INT;
	DECLARE @upgradable_id INT;
	DECLARE @warehouse_id INT;

	DECLARE @first_capacity_level_id INT;
	DECLARE @first_resistance_level_id INT;

	DECLARE @resource_name VARCHAR(255);
	IF @warehouse_type = 'wood_warehouse'
		SET @resource_name = 'wood';
	ELSE IF @warehouse_type = 'food_warehouse'
		SET @resource_name = 'food';
	ELSE IF @warehouse_type = 'stone_warehouse'
		SET @resource_name = 'stone'
	ELSE
		SET @resource_name = 'iron'


	EXEC @worker_id = get_capital_worker @capital_id;
	EXEC @is_worker_available = is_worker_available @worker_id;

	EXEC @first_capacity_level_id = get_first_level_id @warehouse_type, 'capacity'
	EXEC @first_resistance_level_id = get_first_level_id @warehouse_type, 'resistance'

	INSERT INTO Upgradable (upgradable_type) 
		VALUES('warehouse')
	EXEC @upgradable_id = get_last_upgradable_id

	INSERT INTO warehouses (stored, resource_name, capital_id, upgradable_id) 
		VALUES(0, @resource_name, @capital_id, @upgradable_id)
	EXEC @warehouse_id = get_last_warehouse_id;

	INSERT INTO warehouse_Level (warehouse_id, level_id)
		VALUES (@warehouse_id, @first_capacity_level_id)
	INSERT INTO warehouse_Level (warehouse_id, level_id)
		VALUES (@warehouse_id, @first_resistance_level_id)
END
GO
/****** Object:  StoredProcedure [dbo].[create_worker]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[create_worker] (@capital_id INT) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO Workers (capital_id, available) VALUES (@capital_id, 1);
	
END
GO
/****** Object:  StoredProcedure [dbo].[discharge]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[discharge](@producer_id int)
AS
BEGIN 
	BEGIN TRANSACTION
		SET NOCOUNT ON;
		--DECLARE @resource_type int;
		DECLARE @resource_name varchar(255);
		DECLARE @producer_stored int;
		DECLARE @producer_capacity int;
		DECLARE @capital_id int;

		DECLARE @warehouse_id int;
		DECLARE @warehouse_stored int;
		DECLARE @warehouse_capacity int;

		--get producer data
		SELECT @capital_id=p.capital_id, @producer_stored=p.stored, @resource_name=p.resource_name 
			FROM Producers p
			WHERE p.id = @producer_id;
		--get warehouse data
		SELECT @warehouse_id=w.id, @warehouse_stored=w.stored
			FROM Warehouses w 
			WHERE w.capital_id = @capital_id AND w.resource_name = @resource_name;

		EXEC @warehouse_capacity = get_warehouse_capacity @warehouse_id

		IF @producer_stored + @warehouse_stored > @warehouse_capacity
		BEGIN
			DECLARE @transfered int;
			SET @transfered = @warehouse_capacity - @warehouse_stored;
		
			UPDATE Warehouses SET stored = @warehouse_capacity 
				WHERE id = @warehouse_id;
			UPDATE Producers SET stored = @producer_stored - @transfered
				WHERE id = @producer_id;
		END
		ELSE
		BEGIN
			UPDATE Warehouses SET stored += @producer_stored
				WHERE id = @warehouse_id;
			UPDATE Producers SET stored = 0 
				WHERE id = @producer_id;
		END
	COMMIT TRANSACTION;
END
GO
/****** Object:  StoredProcedure [dbo].[discharge_food]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[discharge_food]
	@capital_ID INT
AS
BEGIN
	DECLARE @producer_ID INT;

	EXEC @producer_ID = get_producer_id @capital_id, "food";
	EXEC discharge @producer_ID;
END
GO
/****** Object:  StoredProcedure [dbo].[discharge_iron]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[discharge_iron]
	@capital_ID INT
AS
BEGIN
	DECLARE @producer_ID INT;

	EXEC @producer_ID = get_producer_id @capital_id, "iron";
	EXEC discharge @producer_ID;
END
GO
/****** Object:  StoredProcedure [dbo].[discharge_stone]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[discharge_stone]
	@capital_ID INT
AS
BEGIN
	DECLARE @producer_ID INT;

	EXEC @producer_ID = get_producer_id @capital_id, "stone";
	EXEC discharge @producer_ID;
END
GO
/****** Object:  StoredProcedure [dbo].[discharge_wood]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[discharge_wood] 
	@capital_id INT
AS
BEGIN
	DECLARE @producer_ID INT;

	EXEC @producer_ID = get_producer_id @capital_id, "wood";
	EXEC discharge @producer_ID;
END
GO
/****** Object:  StoredProcedure [dbo].[end_game]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[end_game]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DELETE FROM Workers;
	DELETE FROM Works;

	DELETE FROM Producer_Level;
    DELETE FROM Warehouse_Level;
    DELETE FROM defensive_levels;
    DELETE FROM offensive_level;


    DELETE FROM Producers;
    DELETE FROM Warehouses;
    DELETE FROM defensives;
    DELETE FROM offensive;

	DELETE FROM Upgradable;

	DELETE FROM Capitals;

END
GO
/****** Object:  StoredProcedure [dbo].[find_current_level]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[find_current_level](
	@upgradable_id int, 
	@level_sub_type varchar(255)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @current_level INT;
	DECLARE @temp_id INT;
	DECLARE @upgradable_type VARCHAR(255);

	SELECT @upgradable_type=u.upgradable_type FROM Upgradable u 
		WHERE u.id = @upgradable_id;

    -- Insert statements for procedure here
	IF @upgradable_type = 'producer'
	BEGIN
		SELECT @current_level=l.id
			FROM Producers p, Producer_Level pl, Levels l
			WHERE p.upgradable_id = @upgradable_id AND
				p.id = pl.producer_id AND
				pl.level_id = l.id AND
				l.level_sub_type = @level_sub_type
			ORDER BY l.level_level;
			RETURN @current_level;
	END
	ELSE IF @upgradable_type = 'warehouse'
	BEGIN
		SELECT @current_level=l.id
			FROM Warehouses w, Warehouse_Level wl, Levels l
			WHERE w.upgradable_id = @upgradable_id AND
				w.id = wl.warehouse_id AND
				wl.level_id = l.id AND
				l.level_sub_type = @level_sub_type
			ORDER BY l.level_level;
			RETURN @current_level;

	END
	RETURN @current_level;
END
GO
/****** Object:  StoredProcedure [dbo].[find_next_level]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[find_next_level](@level_id int) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @next_level_id int;
	DECLARE @level_type varchar(255);
	DECLARE @level_sub_type varchar(255);
	DECLARE @current_level_level int;
	DECLARE @next_level_level int;

	-- get current level data
	SELECT @level_type=l.level_type, @level_sub_type=l.level_sub_type, @current_level_level=l.level_level 
		FROM Levels l
		WHERE l.id = @level_id;

	-- get next level data
	SELECT @next_level_id=l.id
		FROM Levels l
		WHERE l.level_type=@level_type AND l.level_sub_type=@level_sub_type AND l.level_level > @current_level_level
		ORDER BY l.level_level;

	RETURN @next_level_id;

END
GO
/****** Object:  StoredProcedure [dbo].[get_available_resource]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[get_available_resource]
	@capital_id INT,
	@resource_name VARCHAR(255)
AS
BEGIN
	DECLARE @overal_amount INT;
	SELECT @overal_amount = w.stored
		FROM Warehouses w
		WHERE w.capital_id = @capital_id AND w.resource_name = @resource_name;
	RETURN @overal_amount;
END
GO
/****** Object:  StoredProcedure [dbo].[get_capital_turn_id]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[get_capital_turn_id] (@capital_id INT)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @turn_id INT;

	SELECT @turn_id = c.turn_id FROM Capitals c 
		WHERE c.id = @capital_id;

	RETURN @turn_id;
END
GO
/****** Object:  StoredProcedure [dbo].[get_capital_worker]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_capital_worker](@capital_id INT)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @worker_id INT;

	SELECT @worker_id=w.id FROM Workers w 
		WHERE w.capital_id = @capital_id;
	RETURN @worker_id;
END
GO
/****** Object:  StoredProcedure [dbo].[get_defensive_resistance]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[get_defensive_resistance] 
	@defensive_id INT
AS
BEGIN
	DECLARE @defensive_resistance INT;

	SELECT @defensive_resistance = SUM(L.resistance)
		FROM defensive_levels WL, Levels L
		WHERE WL.defensive_id = @defensive_id
			AND WL.level_id = L.id
			AND ( (L.level_type = 'trench') OR (L.level_type = 'whirlpool') OR (L.level_type = 'wall') ) 
			AND L.level_sub_type = 'resistance';

	RETURN @defensive_resistance;
END
GO
/****** Object:  StoredProcedure [dbo].[get_defensive_upgradable_id]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_defensive_upgradable_id] (@defensive_id INT)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @upgradable_id INT;
	SELECT @upgradable_id = d.upgradable_id FROM defensives d
		WHERE d.id = @defensive_id;
	RETURN @upgradable_id;

END
GO
/****** Object:  StoredProcedure [dbo].[get_first_level_id]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_first_level_id] (
	@level_type VARCHAR(255),
	@sub_level_type VARCHAR(255)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @level_id INT;

	SELECT @level_id = l.id FROM Levels l
		WHERE l.level_type = @level_type AND
		l.level_sub_type = @sub_level_type AND 
		l.level_level = 1;
	RETURN @level_id;

END
GO
/****** Object:  StoredProcedure [dbo].[get_last_capital]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_last_capital]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE @max_id INT;

	SELECT @max_id = MAX(id) FROM Capitals;
	RETURN @max_id;
END
GO
/****** Object:  StoredProcedure [dbo].[get_last_defensive_id]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[get_last_defensive_id]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @last_id INT;
	SELECT @last_id = MAX(id) FROM defensives;
	RETURN @last_id;

END
GO
/****** Object:  StoredProcedure [dbo].[get_last_offensive_id]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[get_last_offensive_id]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @last_id INT;
	SELECT @last_id = MAX(id) FROM offensive;
	RETURN @last_id;

END
GO
/****** Object:  StoredProcedure [dbo].[get_last_producer_id]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_last_producer_id]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @last_id INT;
	SELECT @last_id = MAX(id) FROM Producers;
	RETURN @last_id;

END
GO
/****** Object:  StoredProcedure [dbo].[get_last_upgradable_id]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_last_upgradable_id]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @max_id INT;

	SELECT @max_id = MAX(id) FROM Upgradable;
	RETURN @max_id;
END
GO
/****** Object:  StoredProcedure [dbo].[get_last_warehouse_id]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_last_warehouse_id]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @last_id INT;
	SELECT @last_id = MAX(id) FROM Warehouses;
	RETURN @last_id;

END
GO
/****** Object:  StoredProcedure [dbo].[get_level_during_time]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_level_during_time](@level_id INT) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @time_to_build INT;
	SELECT @time_to_build = l.time_to_build  FROM Levels l
		WHERE l.id = @level_id;

	RETURN @time_to_build;

END
GO
/****** Object:  StoredProcedure [dbo].[get_offensive_damage]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[get_offensive_damage] 
	@offensive_id INT
AS
BEGIN
	DECLARE @offensive_damage INT;

	SELECT @offensive_damage = SUM(L.damage)
		FROM offensive_level OL, Levels L
		WHERE OL.offensive_id = @offensive_id
			AND OL.level_id = L.id
			AND ( (L.level_type = 'archer_tower') OR (L.level_type = 'fire_tower') OR (L.level_type = 'arrow_tower') OR (L.level_type = 'catapult_tower') ) 
			AND L.level_sub_type = 'damage';

	RETURN @offensive_damage;
END
GO
/****** Object:  StoredProcedure [dbo].[get_offensive_upgradable_id]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_offensive_upgradable_id] (@offensive_id INT)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @upgradable_id INT;
	SELECT @upgradable_id = o.upgradable_id FROM offensive o
		WHERE o.id = @offensive_id;
	RETURN @upgradable_id;

END
GO
/****** Object:  StoredProcedure [dbo].[get_producer_capacity]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[get_producer_capacity] 
	@producer_id INT
AS
BEGIN
	DECLARE @producer_capacity INT;

	SELECT @producer_capacity = SUM(L.capacity)
		FROM Producer_Level PL, Levels L
		WHERE PL.producer_id = @producer_id 
			AND PL.level_id = L.id
			AND ( (L.level_type = 'wood_producer') OR (L.level_type = 'food_producer') OR (L.level_type = 'stone_producer') OR (L.level_type = 'iron_producer') ) 
			AND L.level_sub_type = 'capacity';

	RETURN @producer_capacity;
END
GO
/****** Object:  StoredProcedure [dbo].[get_producer_id]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[get_producer_id] (@capital_id INT, @resource_name VARCHAR(255))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @producer_id INT;
	SELECT @producer_id = p.id FROM Producers p 
		WHERE p.capital_id = @capital_id AND p.resource_name = @resource_name;
	RETURN @producer_id;
END
GO
/****** Object:  StoredProcedure [dbo].[get_producer_level]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_producer_level](@producer_id int) 
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @result int;
	SET @result = (SELECT COUNT(*) FROM Producer_Level
		INNER JOIN Producers ON Producers.id = Producer_Level.producer_id
		WHERE producer_id = @producer_id);
	
	RETURN @result;
END
GO
/****** Object:  StoredProcedure [dbo].[get_producer_resistance]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[get_producer_resistance] 
	@producer_id INT
AS
BEGIN
	DECLARE @producer_resistance INT;

	SELECT @producer_resistance = SUM(L.resistance)
		FROM Producer_Level PL, Levels L
		WHERE PL.producer_id = @producer_id 
			AND PL.level_id = L.id
			AND ( (L.level_type = 'wood_producer') OR (L.level_type = 'food_producer') OR (L.level_type = 'stone_producer') OR (L.level_type = 'iron_producer') ) 
			AND L.level_sub_type = 'resistance';

	RETURN @producer_resistance;
END
GO
/****** Object:  StoredProcedure [dbo].[get_producer_speed]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[get_producer_speed] 
	@producer_id INT
AS
BEGIN

	DECLARE @producer_speed INT;

	SELECT @producer_speed = SUM(L.speed)
		FROM Producer_Level PL, Levels L
		WHERE PL.producer_id = @producer_id
			AND L.id = PL.level_id
			AND L.level_sub_type = 'speed';

	RETURN @producer_speed;
END
GO
/****** Object:  StoredProcedure [dbo].[get_producer_upgradable_id]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_producer_upgradable_id] (@producer_id INT)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @upgradable_id INT;
	SELECT @upgradable_id = p.upgradable_id FROM Producers p
		WHERE p.id = @producer_id;
	RETURN @upgradable_id;

END
GO
/****** Object:  StoredProcedure [dbo].[get_required_resource_amount]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[get_required_resource_amount] (@level_id INT) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @resource_amount INT;

    SELECT @resource_amount = l.resource_required_value FROM Levels l
		WHERE l.id = @level_id;

	RETURN @resource_amount;
END
GO
/****** Object:  StoredProcedure [dbo].[get_required_resource_id]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[get_required_resource_id] (@level_id INT) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @resource_id INT;

    SELECT @resource_id = l.resource_required_id FROM Levels l
		WHERE l.id = @level_id;

	RETURN @resource_id;
END
GO
/****** Object:  StoredProcedure [dbo].[get_resource_amount]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[get_resource_amount](@resource_name VARCHAR(255))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @resource_amount int;

    -- Insert statements for procedure here
	SELECT @resource_amount = w.stored FROM Warehouses w
		WHERE w.resource_name = @resource_name;
	
	RETURN @resource_amount;
END
GO
/****** Object:  StoredProcedure [dbo].[get_resource_name]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[get_resource_name]
(
	@resource_id INT,
	@resource_name VARCHAR(255) OUTPUT
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT @resource_name = r.resource_name FROM Resources r 
		WHERE r.id = @resource_id;
END
GO
/****** Object:  StoredProcedure [dbo].[get_upgradable_capital]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_upgradable_capital](@upgradable_id INT)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE @upgradable_type VARCHAR(255);
	DECLARE @capital_id INT;

	SELECT @upgradable_type = u.upgradable_type 
		FROM Upgradable u Where u.id = @upgradable_id;

	IF @upgradable_type = 'producer'
	BEGIN
		SELECT @capital_id=p.capital_id FROM Producers p 
			WHERE p.upgradable_id=@upgradable_id;
			
	END
	ELSE IF @upgradable_type = 'warehouse'
	BEGIN
		SELECT @capital_id=W.capital_id FROM Warehouses w 
			WHERE w.upgradable_id=@upgradable_id;
	END
	RETURN @capital_id
END
GO
/****** Object:  StoredProcedure [dbo].[get_warehouse_capacity]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[get_warehouse_capacity] 
	@warehouse_id INT
AS
BEGIN

	DECLARE @warehouse_capacity INT;
	SELECT @warehouse_capacity = SUM(L.capacity)
		FROM Warehouse_Level wl, Levels l
		WHERE wl.warehouse_id = @warehouse_id
			AND wl.level_id = l.id
			AND l.level_sub_type = 'capacity';

	RETURN @warehouse_capacity;
END
GO
/****** Object:  StoredProcedure [dbo].[get_warehouse_id]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[get_warehouse_id] (@capital_id INT, @resource_name VARCHAR(255))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @warehouse_id INT;
	SELECT @warehouse_id = w.id FROM Warehouses w
		WHERE w.capital_id = @capital_id AND w.resource_name = @resource_name;
	RETURN @warehouse_id;
END
GO
/****** Object:  StoredProcedure [dbo].[get_warehouse_level]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_warehouse_level](@warehouse_id int) 
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @result int;
	SET @result = (SELECT COUNT(*) FROM Warehouse_Level
		INNER JOIN Warehouses ON Warehouses.id = Warehouse_Level.warehouse_id
		WHERE warehouse_id = @warehouse_id);
	
	RETURN @result;
END
GO
/****** Object:  StoredProcedure [dbo].[get_warehouse_resistance]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[get_warehouse_resistance] 
	@warehouse_id INT
AS
BEGIN
	DECLARE @warehouse_capacity INT;

	SELECT @warehouse_capacity = SUM(L.capacity)
		FROM Warehouse_Level WL, Levels L
		WHERE WL.warehouse_id = @warehouse_id 
			AND WL.level_id = L.id
			AND ( (L.level_type = 'wood_warehouse') OR (L.level_type = 'food_warehouse') OR (L.level_type = 'stone_warehouse') OR (L.level_type = 'iron_warehouse') ) 
			AND L.level_sub_type = 'capacity';

	RETURN @warehouse_capacity;
END
GO
/****** Object:  StoredProcedure [dbo].[get_warehouse_upgradable_id]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[get_warehouse_upgradable_id] (@warehouse_id INT)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @upgradable_id INT;
	SELECT @upgradable_id = w.upgradable_id FROM Warehouses w
		WHERE w.id = @warehouse_id;
	RETURN @upgradable_id;

END
GO
/****** Object:  StoredProcedure [dbo].[is_worker_available]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[is_worker_available](@worker_id INT)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @available INT

	SELECT @available = w.available FROM Workers w WHERE w.id = @worker_id
	IF @available = 1 
		RETURN 1
	ELSE 
		RETURN 0
END
GO
/****** Object:  StoredProcedure [dbo].[partial_upgrade]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[partial_upgrade](@upgradable_id INT, @level_id INT)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @upgradable_type VARCHAR(255);
	SELECT @upgradable_type=u.upgradable_type FROM Upgradable u
		WHERE u.id = @upgradable_id;

	IF @upgradable_type = 'producer'
	BEGIN
		DECLARE @producer_id INT;
		SELECT @producer_id=p.id FROM Producers p
			WHERE p.upgradable_id=@upgradable_id;
		INSERT INTO Producer_Level (producer_id, level_id)
			VALUES (@producer_id, @level_id)
			
	END
	ELSE IF @upgradable_type = 'warehouse'
	BEGIN
		DECLARE @warehouse_id INT;
		SELECT @warehouse_id=w.id FROM Warehouses w
			WHERE w.upgradable_id=@upgradable_id;
		INSERT INTO Warehouse_Level(warehouse_id, level_id)
			VALUES (@warehouse_id, @level_id)
	END
END
GO
/****** Object:  StoredProcedure [dbo].[produce_resource]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[produce_resource]
AS
BEGIN

	DECLARE @producer_id INT;
	DECLARE @producer_stored INT;

	DECLARE get_producers_cursor CURSOR LOCAL FOR 
		SELECT id FROM Producers;
	OPEN get_producers_cursor;
	FETCH NEXT FROM get_producers_cursor INTO @producer_id WHILE @@FETCH_STATUS = 0
	BEGIN

		DECLARE @producer_speed INT;
		DECLARE @producer_capacity INT;

		EXEC @producer_speed = get_producer_speed @producer_id;
		EXEC @producer_capacity = get_producer_capacity @producer_id;
		SELECT @producer_stored = P.stored FROM Producers P WHERE P.id = @producer_id;

		IF @producer_speed + @producer_stored > @producer_capacity
			UPDATE Producers SET stored = @producer_capacity WHERE @producer_id = id;
		ELSE
			UPDATE Producers SET stored = stored + @producer_speed WHERE @producer_id = id;

		FETCH NEXT FROM get_producers_cursor INTO @producer_id;
	END
END
GO
/****** Object:  StoredProcedure [dbo].[reduce_resource_from_warehouse]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[reduce_resource_from_warehouse]
	@capital_id INT,
	@resource_name VARCHAR(255),
	@resource_amount INT
AS
BEGIN
	DECLARE @available_value INT;
	EXEC @available_value = get_available_resource @capital_id, @resource_name;
	IF @resource_amount < @available_value BEGIN
		UPDATE Warehouses SET stored = (stored - @resource_amount) WHERE capital_id = @capital_id;
		RETURN 1;
	END
	RETURN 0;
END
GO
/****** Object:  StoredProcedure [dbo].[remove_work]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[remove_work](@work_id INT)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DELETE FROM Works
		WHERE id = @work_id;
END
GO
/****** Object:  StoredProcedure [dbo].[set_worker_availability]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[set_worker_availability](@worker_id INT, @availability INT) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE Workers 
		SET available=@availability
		WHERE id=@worker_id;
END
GO
/****** Object:  StoredProcedure [dbo].[start_game]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[start_game]
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @capital_id INT;
	EXEC @capital_id = create_capital;

	EXEC create_worker @capital_id

	EXEC create_producer 'wood_producer', @capital_id
	EXEC create_producer 'food_producer', @capital_id
	EXEC create_producer 'stone_producer', @capital_id
	EXEC create_producer 'iron_producer', @capital_id

	EXEC create_warehouse 'wood_warehouse', @capital_id
	EXEC create_warehouse 'food_warehouse', @capital_id
	EXEC create_warehouse 'stone_warehouse', @capital_id
	EXEC create_warehouse 'iron_warehouse', @capital_id

	EXEC create_defensive 'trench', @capital_id
	EXEC create_defensive 'whirlpool', @capital_id
	EXEC create_defensive 'wall', @capital_id

	EXEC create_offensive'archer_tower', @capital_id
	EXEC create_offensive 'fire_tower', @capital_id
	EXEC create_offensive 'arrow_tower', @capital_id
	EXEC create_offensive 'catapult_tower', @capital_id

	RETURN @capital_id;
END
GO
/****** Object:  StoredProcedure [dbo].[start_upgrade]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[start_upgrade](
	@upgradable_id INT, 
	@level_sub_type VARCHAR(255)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @capital_id INT;
	DECLARE @worker_id INT;
	DECLARE @time_to_build INT;
	DECLARE @current_level_id INT;
	DECLARE @next_level_id INT;
	DECLARE @turn_id INT;
	DECLARE @resource_required_id INT;
	DECLARE @resource_required_amount INT;
	DECLARE @resource_required_name VARCHAR(255);
	DECLARE @available_resource INT;

	DECLARE @is_worker_available INT;
	DECLARE @check_turn INT;

	EXEC @capital_id = get_upgradable_capital @upgradable_id;
	EXEC @worker_id = get_capital_worker @capital_id;
	EXEC @is_worker_available = is_worker_available @worker_id;
	
	EXEC @current_level_id = find_current_level @upgradable_id, @level_sub_type;
	EXEC @next_level_id = find_next_level @current_level_id;
	EXEC @time_to_build = get_level_during_time @next_level_id;
	EXEC @turn_id = get_capital_turn_id @capital_id
	EXEC @check_turn = check_turn @next_level_id, @turn_id;
	EXEC @resource_required_id = get_required_resource_id @next_level_id;
	EXEC @resource_required_amount = get_required_resource_amount @next_level_id;
	EXEC get_resource_name @resource_required_id, @resource_required_name OUTPUT;
	EXEC @available_resource = get_available_resource @capital_id, @resource_required_name
	PRINT @available_resource;
	PRINT @resource_required_amount;

	IF @is_worker_available = 1 AND @check_turn = 1 AND (@available_resource >= @resource_required_amount)
	BEGIN
		EXEC reduce_resource_from_warehouse @capital_id, @resource_required_name, @resource_required_amount;
		EXEC set_worker_availability @worker_id, 0
		INSERT INTO Works (upgradable_id, worker_id, time_to_build, level_id, started_time) 
			VALUES (@upgradable_id, @worker_id, @time_to_build, @next_level_id, GETDATE())
		RETURN 1;
	END
	RETURN 0;
END
GO
/****** Object:  StoredProcedure [dbo].[upgrade]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[upgrade]
(
	@upgradable_id INT,	
	@level_sub_type VARCHAR(255)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
		
	DECLARE @worker_id INT;
	DECLARE @capital_id INT;
	--DECLARE @is_worker_available INT;

	EXEC @capital_id=get_upgradable_capital @upgradable_id
	EXEC @worker_id=get_capital_worker @capital_id
	--EXEC @is_worker_available = is_worker_available @worker_id
	
	DECLARE @current_level_id INT;
	DECLARE @next_level_id INT;
	--EXEC set_worker_availability @worker_id, 0;
	EXEC @current_level_id=find_current_level @upgradable_id, @level_sub_type
	EXEC @next_level_id=find_next_level @current_level_id
		
	--DECLARE @delay_length char(8) = '00:00:10';
	--WAITFOR DELAY @delay_length
		
	EXEC set_worker_availability @worker_id, 1
	EXEC partial_upgrade @upgradable_id, @next_level_id

	RETURN 1;
END
GO
/****** Object:  StoredProcedure [dbo].[upgrade_turn]    Script Date: 1/11/2018 10:56:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[upgrade_turn] (@capital_id INT)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @turn_id INT;
	SELECT @turn_id = c.turn_id FROM Capitals c WHERE c.id = @capital_id;
	
	DECLARE @resource_id INT;
	DECLARE @resource_amount INT;

	SELECT @resource_id = t.resource_require_id, @resource_amount = t.resource_required_value  
		FROM Turns t 
		WHERE t.id = (@turn_id + 1);

	DECLARE @resource_name VARCHAR(255);
	SELECT @resource_name = r.resource_name FROM Resources r 
		WHERE r.id = @resource_id;

	DECLARE @available_resource INT;
	EXEC @available_resource = get_available_resource @capital_id, @resource_name;
	
	UPDATE Capitals
		SET turn_id = (@turn_id + 1);
		
END
GO
USE [master]
GO
ALTER DATABASE [XaravanDB] SET  READ_WRITE 
GO
