DECLARE @producer_id INT;
DECLARE @producer_stored INT;

DECLARE get_producers_cursor CURSOR LOCAL FOR 
	SELECT id FROM Producers;
OPEN get_producers_cursor;
FETCH NEXT FROM get_producers_cursor INTO @producer_id WHILE @@FETCH_STATUS = 0
BEGIN

	DECLARE @producer_speed INT;
	DECLARE @producer_capacity INT;

	EXEC @producer_speed = get_producer_speed @producer_id;
	EXEC @producer_capacity = get_producer_capacity @producer_id;
	SELECT @producer_stored = P.stored FROM Producers P WHERE P.id = @producer_id;

	IF @producer_speed + @producer_stored > @producer_capacity
		UPDATE Producers SET stored = @producer_capacity WHERE @producer_id = id;
	ELSE
		UPDATE Producers SET stored = stored + @producer_speed WHERE @producer_id = id;

	FETCH NEXT FROM get_producers_cursor INTO @producer_id;
END