
DECLARE @started_time DATETIME;
DECLARE @time_to_build INT;
DECLARE @worker_id INT;
DECLARE @work_id INT;
DECLARE @during INT;
DECLARE @upgradable_id INT;
DECLARE @next_level_id INT;

--SELECT @time_to_build=w.time_to_build, @started_time=w.started_time FROM Works w ;

DECLARE myCursor CURSOR LOCAL FOR 
	SELECT id, time_to_build, started_time , worker_id, upgradable_id, level_id
	FROM Works;

	OPEN myCursor
		FETCH NEXT FROM myCursor INTO 
			@work_id, @time_to_build, @started_time, @worker_id, @upgradable_id, @next_level_id
		WHILE @@FETCH_STATUS = 0
		BEGIN
			SELECT @during=DATEDIFF(MINUTE, @started_time, GETDATE());
			IF @time_to_build - @during <= 0
			BEGIN
				-- UPGRADE
				EXEC set_worker_availability @worker_id, 1;
				EXEC remove_work @work_id; 
				EXEC add_level @upgradable_id, @next_level_id;
				PRINT (@time_to_build - @during);

			END
			ELSE
			BEGIN
				-- DON'T UPGRADE
				PRINT 'NOT DONE';
			END
			FETCH NEXT FROM myCursor INTO 
				@work_id, @time_to_build, @started_time, @worker_id, @upgradable_id, @next_level_id;
		END

	CLOSE myCursor
DEALLOCATE myCursor
