﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Text.RegularExpressions;

namespace XaravanaUI
{
    class Program
    {
        static void Main(string[] args)
        {
            string connectionString = null;
            SqlCommand command = null;
            SqlConnection cnn;
            connectionString = "Data Source=DESKTOP-9GUESDP;Initial Catalog=XaravanDB;Integrated Security=True";
            cnn = new SqlConnection(connectionString);
            string sql;

            int capital_id;

            sql = "start_game";
            command = new SqlCommand(sql, cnn);
            command.CommandType = CommandType.StoredProcedure;
            var returnParameter = command.Parameters.Add("@capital_id", SqlDbType.Int);
            returnParameter.Direction = ParameterDirection.ReturnValue;
            cnn.Open();
            command.ExecuteNonQuery();
            capital_id = (int) returnParameter.Value;
            command.Dispose();
            cnn.Close();
            Console.WriteLine("new game started\n" + "your capital id is:" + capital_id.ToString());
            while(true)
            {
                Console.WriteLine("for getting status press 1 \n" +
                "for discharging press 2 \n" +
                "for upgrading press 3 \n" +
                "for attacking press 4 \n" +
                "for exiting press 0");
                string result = Console.ReadLine();
                if(result == "1")
                {

                }
                else if(result == "2")
                {
                    Console.WriteLine("please insert dischargeing info \n" +
                        "Eg. wood");
                    string dischargeInfo = Console.ReadLine();
                    sql = "DECLARE @producer_id int;\n" +
                        "EXEC @producer_id = get_producer_id " + capital_id.ToString() + ", '" + dischargeInfo +
                        "'; \n EXEC discharge @producer_id;\n";
                    command = new SqlCommand(sql, cnn);
                    command.CommandType = CommandType.Text;
                    cnn.Open();
                    command.ExecuteNonQuery();
                    cnn.Close();
                    command.Dispose();
                }
                else if(result == "3")
                {
                    Console.WriteLine("please insert your upgrading info (building_type resource_type item) \n" +
                                    "Eg. producer wood speed\n" +
                                    "Eg. warehouse stone capacity\n" +
                                    "Eg. offensive fire_tower resistance\n" +
                                    "Eg. turn\n");
                    string upgradeInfo;
                    upgradeInfo = Console.ReadLine();
                    string[] elements = upgradeInfo.Split(default(String[]), StringSplitOptions.RemoveEmptyEntries);
                    Console.WriteLine("upgrading");
                    if(elements[0] == "producer")
                    {
                        sql = "DECLARE @producer_id int;\n DECLARE @upgradable_id int;\n" +
                            "EXEC @producer_id = get_producer_id " + capital_id.ToString() + ", '" + elements[1] +
                            "'; \n EXEC @upgradable_id = get_producer_upgradable_id @producer_id;\n" +
                            "EXEC start_upgrade @upgradable_id, '" + elements[2] + "' \n";
                        command = new SqlCommand(sql, cnn);
                        command.CommandType = CommandType.Text;
                        cnn.Open();
                        command.ExecuteNonQuery();
                        cnn.Close();
                        command.Dispose();
                    }
                    else if(elements[0] == "warehouse")
                    {
                        sql = "DECLARE @warehouse_id int;\n DECLARE @upgradable_id int;\n" +
                            "EXEC @producer_id = get_warehouse_id " + capital_id.ToString() + ", '" + elements[1] +
                            "'; \n EXEC @upgradable_id = get_warehouse_upgradable_id @producer_id;\n" +
                            "EXEC start_upgrade @upgradable_id, '" + elements[2] + "' \n";
                        command = new SqlCommand(sql, cnn);
                        command.CommandType = CommandType.Text;
                        cnn.Open();
                        command.ExecuteNonQuery();
                        cnn.Close();
                        command.Dispose();
                    }
                    else if(elements[0] == "offensive")
                    {
                        sql = "DECLARE @offensive_id int;\n DECLARE @upgradable_id int;\n" +
                            "EXEC @offensive_id = get_offensive_id " + capital_id.ToString() + ", '" + elements[1] +
                            "'; \n EXEC @upgradable_id = get_offensive_upgradable_id @offensive_id;\n" +
                            "EXEC start_upgrade @upgradable_id, '" + elements[2] + "' \n";
                        command = new SqlCommand(sql, cnn);
                        command.CommandType = CommandType.Text;
                        cnn.Open();
                        command.ExecuteNonQuery();
                        cnn.Close();
                        command.Dispose();
                    }
                    else if(elements[0] == "defensive")
                    {
                        sql = "DECLARE @defensive_id int;\n DECLARE @upgradable_id int;\n" +
                            "EXEC @defensive_id = get_defensive_id " + capital_id.ToString() + ", '" + elements[1] +
                            "'; \n EXEC @upgradable_id = get_defensive_upgradable_id @defensive_id;\n" +
                            "EXEC start_upgrade @upgradable_id, '" + elements[2] + "' \n";
                        command = new SqlCommand(sql, cnn);
                        command.CommandType = CommandType.Text;
                        cnn.Open();
                        command.ExecuteNonQuery();
                        cnn.Close();
                        command.Dispose();
                    }
                    else if(elements[0] == "turn")
                    {
                        sql = "EXEC upgrade_turn " + capital_id.ToString() + ";";
                        command = new SqlCommand(sql, cnn);
                        command.CommandType = CommandType.Text;
                        cnn.Open();
                        command.ExecuteNonQuery();
                        cnn.Close();
                        command.Dispose();
                    }
                }
                else if(result == "4")
                {
                    Console.WriteLine("please insert what capital id you want to attack. \n");
                    string defender_capital_id = Console.ReadLine();
                    sql = "EXEC attack " + capital_id.ToString() + ", " + defender_capital_id + ";";
                    command = new SqlCommand(sql, cnn);
                    command.CommandType = CommandType.Text;
                    cnn.Open();
                    command.ExecuteNonQuery();
                    cnn.Close();
                    command.Dispose();
                }
                else if(result == "0")
                {
                    Console.WriteLine("exiting the game");
                    sql = "end_game";
                    command = new SqlCommand(sql, cnn);
                    command.CommandType = CommandType.StoredProcedure;
                    cnn.Open();
                    command.ExecuteNonQuery();
                    command.Dispose();
                    cnn.Close();
                    break;
                }
                else
                {
                    Console.WriteLine("incorrect input");
                }
            }
        }
        
    }
}
